import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-rotating-logo',
  templateUrl: './rotating-logo.component.html',
  styleUrls: ['./rotating-logo.component.css'],
  animations: [
    /** TRIGGER: contenitore di animazioni */
    trigger('rotate', [
      /**
       * STATE: gli stati della mia animazione
       * STYLE: regole css dell'animazione
       */
      state('state1', style({
        transform: 'rotate(0)'
      })),
      state('state2', style({
        transform: 'rotate(180deg)'
      })),
      /** Definizione transizione da stato1 a stato 2 in 1s e viceversa */
      transition('state1=>state2', animate('1000ms')),
      transition('state2=>state1', animate('1000ms')),
    ])
  ]
})
export class RotatingLogoComponent implements OnInit {
  current = 'state1';
  constructor() { }

  ngOnInit() {
  }
  twist1() {
    /**
     * if(cond){
     *     c=a
     * } else {
     *     c=b
     * }
     */
    this.current = this.current === 'state1' ? 'state2' : 'state1';
  }
  twist() {
    this.current = this.current === 'state1' ? 'state2' : 'state1';
  }
}
