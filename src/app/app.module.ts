import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RotatingLogoComponent} from './rotating-logo/rotating-logo.component';
import {PannelloComponent} from './pannello/pannello.component';

@NgModule({
    declarations: [
        AppComponent,
        RotatingLogoComponent,
        PannelloComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
