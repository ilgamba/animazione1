import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-pannello',
  templateUrl: './pannello.component.html',
  styleUrls: ['./pannello.component.css'],
  animations: [
    trigger('svela', [
      state('off', style({
        opacity: '0',
        height: '0',
      })),
      state('on', style({
        opacity: '1',
        height: '*',
      })),
      /** DOPPIA FRECCIA PER TRANSITION BIDIREZIONALE */
      transition('off<=>on', animate('500ms')),
    ])
  ]
})
export class PannelloComponent implements OnInit {
  current = 'off';
  constructor() { }

  ngOnInit() {
  }

  svela() {
    this.current = this.current === 'off' ? 'on' : 'off';
  }
}
